'use strict';

var config = require('../config.json');
var aerospikeStore = require('../../index');
var sinon = require('sinon');
var assert = require('assert');
var clone = require('clone');

var aerospikeCache;
var customRedisCache;

var aerospikeDefaultOptions = {
  "ns": "rx",
  "set": "test"
};

before(function () {
  aerospikeCache = require('cache-manager').caching({
    store: aerospikeStore,
    host: config.aerospike.host,
    port: config.aerospike.port
  });
});

describe ('initialization', function () {
  it('should create a connection with defaults', function(done) {
    var aerospikeConnectionTestCache = require('cache-manager').caching({
      store: aerospikeStore
    });
    done();
  });
});

describe('set', function () {
  it('should store a value without ttl', function (done) {
    aerospikeCache.set('foo', 'bar', aerospikeDefaultOptions, function (err) {
      assert.equal(err, null);
      done();
    });
  });

  it('should store a value with a specific ttl', function (done) {
    var options = clone(aerospikeDefaultOptions);
    options['ttl'] = config.aerospike.ttl;
    aerospikeCache.set('foo', 'bar', options, function (err) {
      assert.equal(err, null);
      done();
    });
  });

  it('should store a null value without error', function (done) {
    aerospikeCache.set('foo2', null, aerospikeDefaultOptions, function (err) {
      try {
        assert.equal(err, null);
        aerospikeCache.get('foo2', aerospikeDefaultOptions, function (err, value) {
          assert.equal(err, null);
          assert.equal(value, null);
          done();
        });
      } catch (e) {
        done(e);
      }
    });
  });

  it('should store a value without callback', function (done) {
    aerospikeCache.set('foo', 'baz', aerospikeDefaultOptions);
    aerospikeCache.get('foo', aerospikeDefaultOptions, function (err, value) {
      assert.equal(err, null);
      assert.equal(value, 'baz');
      done();
    });
  });

  it('should not store an invalid value', function (done) {
    aerospikeCache.set('foo1', undefined, aerospikeDefaultOptions, function (err) {
      try {
        assert.notEqual(err, null);
        assert.equal(err.message, 'value cannot be undefined');
        done();
      } catch (e) {
        done(e);
      }
    });
  });

  it('should return an error if Aerospike ns and set are not specified', function (done) {
    aerospikeCache.set('foo', 'baz', function (err) {
      assert.notEqual(err, null);
      assert.equal(err.message, 'Missing Aerospike namespace and set in options.');
      done();
    });
  });

  it('should return an error if options are null', function (done) {
    aerospikeCache.set('foo', 'baz', null, function (err) {
      assert.notEqual(err, null);
      assert.equal(err.message, 'Missing Aerospike namespace and set in options.');
      done();
    });
  });

  it('should return an error if there is an error acquiring a connection', function (done) {
    var aerospike = aerospikeCache.store._aerospike;
    aerospikeCache.store.connection.close(false);
    aerospikeCache.store.connection = false;

    sinon.stub(aerospike, 'connect', function (args, cb) {cb(new Error("Failed Test Connection"))});
    aerospikeCache.set('foo', 'baz', aerospikeDefaultOptions, function (err) {
      aerospike.connect.restore();
      assert.notEqual(err, null);
      done();
    });
  });
});

describe('get', function () {
  it('should retrieve a value for a given key', function (done) {
    var value = 'bar';
    aerospikeCache.set('foo', value, aerospikeDefaultOptions, function () {
      aerospikeCache.get('foo', aerospikeDefaultOptions, function (err, result) {
        assert.equal(err, null);
        assert.equal(result, value);
        done();
      });
    });
  });

  it('should retrieve an object for a given key', function (done) {
    var value = {'foo': 'bar'};
    aerospikeCache.set('foo', value, aerospikeDefaultOptions, function () {
      aerospikeCache.get('foo', aerospikeDefaultOptions, function (err, result) {
        assert.equal(err, null);
        assert.deepEqual(result, value);
        done();
      });
    });
  });

  it('should return null when the key is invalid', function (done) {
    aerospikeCache.get('invalidKey', aerospikeDefaultOptions, function (err, result) {
      assert.equal(err, null);
      assert.equal(result, null);
      done();
    });
  });

  it('should return an error if there is an error', function (done) {
    sinon.stub(aerospikeCache.store.connection, 'get', function (args, cb) {cb(new Error("Failed Test Get"))});
    aerospikeCache.get('foo', aerospikeDefaultOptions, function (err) {
      aerospikeCache.store.connection.get.restore();
      assert.notEqual(err, null);
      done();
    });
  });

  it('should return an error if Aerospike ns and set are not specified', function (done) {
    aerospikeCache.get('foo', function (err) {
      assert.notEqual(err, null);
      assert.equal(err.message, 'Missing Aerospike namespace and set in options.');
      done();
    });
  });

  it('should return an error if there is an error acquiring a connection', function (done) {
    var aerospike = aerospikeCache.store._aerospike;
    aerospikeCache.store.connection.close(false);
    aerospikeCache.store.connection = false;

    sinon.stub(aerospike, 'connect', function (args, cb) {cb(new Error("Failed Test Connection"))});
    aerospikeCache.get('foo', aerospikeDefaultOptions, function (err) {
      aerospike.connect.restore();
      assert.notEqual(err, null);
      done();
    });
  });
});

describe('del', function () {
  it('should delete a value for a given key', function (done) {
    aerospikeCache.set('foo', 'bar', aerospikeDefaultOptions, function () {
      aerospikeCache.del('foo', aerospikeDefaultOptions, function (err) {
        assert.equal(err, null);
        done();
      });
    });
  });

  it('should delete a value for a given key without callback', function (done) {
    aerospikeCache.set('foo', 'bar', aerospikeDefaultOptions, function () {
      aerospikeCache.del('foo', aerospikeDefaultOptions);
      done();
    });
  });

  it('should return an error if Aerospike ns and set are not specified', function (done) {
    aerospikeCache.del('foo', function (err) {
      assert.notEqual(err, null);
      assert.equal(err.message, 'Missing Aerospike namespace and set in options.');
      done();
    });
  });

  it('should return an error if there is an error acquiring a connection', function (done) {
    aerospikeCache.store.connection.close(false);
    aerospikeCache.store.connection = false;

    var aerospike = aerospikeCache.store._aerospike;
    sinon.stub(aerospike, 'connect', function (args, cb) {cb(new Error("Failed Test Connection"))});
    aerospikeCache.del('foo', aerospikeDefaultOptions, function (err) {
      aerospike.connect.restore();
      assert.notEqual(err, null);
      done();
    });
  });
});
