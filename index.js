'use strict';

var Aerospike = require('aerospike');
const Key = Aerospike.Key;

/**
 * The cache manager Aerospike Store module
 * @module aerospikeStore
 * @param {Object} [args] - The store configuration (optional)
 */
function aerospikeStore(args) {

  var self = {
    name: 'aerospike',
    connection: false
  };

  // cache-manager should always pass in args
  /* istanbul ignore next */
  var aeroOptions = args || {};

  aeroOptions.host = aeroOptions.host || '127.0.0.1';
  aeroOptions.port = aeroOptions.port || 3000;
  aeroOptions.hosts = aeroOptions.host + ":" + aeroOptions.port;

  /* istanbul ignore next */
  if( aeroOptions.live_connection ) {
    self.connection = aeroOptions.live_connection;
  }

  /**
   * Helper to connect to a connection
   * @private
   * @param {Function} cb - A callback that returns
   */
  function connect(cb) {
    if (self.connection) {
      return cb(null, self.connection);
    }

    Aerospike.connect(aeroOptions, function (err, client) {
      if (err) {
        return cb(err);
      }
      self.connection = client;
      cb(null, client);
    })
  }

  /**
   * Get a value for a given key.
   * @method get
   * @param {String} key - The cache key
   * @param {Object} [options] - The options (optional)
   * @param {Function} cb - A callback that returns a potential error and the response
   */
  self.get = function(key, options, cb) {
    connect(function (err, client) {
      if (err) {
        return cb(err);
      }

      if (typeof options === 'function') {
        cb = options;
      }

      if (! (options.ns && options.set)) {
        return cb(new Error("Missing Aerospike namespace and set in options."));
      }

      var keyObject = new Key(options.ns, options.set, key);
      client.get(keyObject, function (err, record, meta) {
        if (err) {
          if (err.message == "AEROSPIKE_ERR_RECORD_NOT_FOUND") {
            return cb(null, null);
          }
          return cb(err, null);
        }

        var returnRecord = record;

        if (record["__nonObject"]) {
          returnRecord = record["__nonObject"];
        } else if (record["__null"]) {
          returnRecord = null;
        }

        cb(null, returnRecord);
      });
    });
  };

  /**
   * Set a value for a given key.
   * @method set
   * @param {String} key - The cache key
   * @param {String} value - The value to set
   * @param {Object} [options] - The options (optional)
   * @param {Object} options.ttl - The ttl value
   * @param {Function} [cb] - A callback that returns a potential error, otherwise null
   */
  self.set = function(key, value, options, cb) {
    connect(function(err, client) {
      if (err) {
        return cb(err);
      }

      if (typeof options === 'function') {
        cb = options;
        options = {};
      }

      options = options || {};

      if (!cb) {
        cb = function() {};
      }

      if (undefined === value) {
        return cb(new Error("value cannot be undefined"));
      }

      if (! (options.ns && options.set)) {
        return cb(new Error("Missing Aerospike namespace and set in options."));
      }

      var keyObject = new Key(options.ns, options.set, key);

      var val = value;
      if (("string" == typeof value) || (Array.isArray(value))) {
        val = {"__nonObject": value};
      } else if (null === value) {
        val = {"__null": "true"};
      }

      var meta = {};
      if (options['ttl']) {
        meta['ttl'] = options['ttl'];
      }
      var policy = { "exists": Aerospike.policy.exists.CREATE_OR_REPLACE };

      client.put(keyObject, val, meta, policy, cb);
    });
  };

  /**
   * Delete value of a given key
   * @method del
   * @param {String} key - The cache key
   * @param {Object} [options] - The options (optional)
   * @param {Function} [cb] - A callback that returns a potential error, otherwise null
   */
  self.del = function(key, options, cb) {
    connect(function(err, client) {
      if (err) {
        return cb(err);
      }

      if (typeof options === 'function') {
        cb = options;
        options = {};
      }

      if (! (options.ns && options.set)) {
        return cb(new Error("Missing Aerospike namespace and set in options."));
      }

      var keyObject = new Key(options.ns, options.set, key);
      client.remove(keyObject, function(error, key) {
        cb(error);
      });
    });
  };

  /**
   * Expose the raw aerospike object for testing purposes
   * @private
   */
  self._aerospike = Aerospike;

  return self;
}

module.exports = {
  create: function(args) {
    return aerospikeStore(args);
  }
};
